package com.example.thomasraybould.nycschools.di.app_component;

import javax.inject.Scope;

@Scope
public @interface AppScope {
}
